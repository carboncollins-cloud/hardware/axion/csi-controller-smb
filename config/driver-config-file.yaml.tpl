{{ with secret "c3kv/data/datacenter/soc/storage/axion/csi" }}
driver: smb-client
instance_id: {{ env "NOMAD_ALLOC_ID" }}
smb:
  shareHost: {{ index .Data.data "host" }}
  shareBasePath: "{{ index .Data.data "share" }}"
  controllerBasePath: "/storage"
  dirPermissionsMode: "0777"
  dirPermissionsUser: 3205
  dirPermissionsGroup: 3205

_private:
  csi:
    volume:
      idHash:
        strategy: crc16
{{ end }}
