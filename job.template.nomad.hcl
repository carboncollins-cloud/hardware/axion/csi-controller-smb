locals {
  csi_mode = "controller"
  csi_name = "soc-axion-smb"
  csi_version = "[[ .csiVersion ]]"
}

job "storage-plugin-axion-smb-controller" {
  name = "Storage Plugin Controller (SoC Axion SMB)"
  type = "service"
  region = "se"
  datacenters = ["soc"]
  namespace = "c3-hardware-axion"

  priority = 80

  group "controller" {
    count = 3

    constraint {
      distinct_hosts = true
    }

    consul {}

    service {
      name = "soc-axion-smb-csi-controller"
      task = "plugin"
    }

    task "plugin" {
      driver = "docker"

      vault {
        role = "service-axion-csi-controller"
      }

      config {
        image = "[[ .democraticImage ]]"

        args = [
          "--csi-mode=${local.csi_mode}",
          "--csi-version=${local.csi_version}",
          "--csi-name=${local.csi_name}",
          "--driver-config-file=${NOMAD_TASK_DIR}/driver-config-file.yaml",
          "--server-socket=unix:///csi/csi.sock",
          "--log-level=debug"
        ]
      }

      csi_plugin {
        id = "${local.csi_name}"
        type = "${local.csi_mode}"
        mount_dir = "/csi"
        health_timeout = "30s"
      }

      template {
        data = <<EOH
[[ fileContents "./config/driver-config-file.yaml.tpl" ]]
        EOH

        destination = "${NOMAD_TASK_DIR}/driver-config-file.yaml"
      }

      resources {
        cpu = 100
        memory = 100
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "30s"
    healthy_deadline = "5m"
    progress_deadline = "10m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
